<?php

require_once "model/ModelInterface.class.php";
require_once "model/persist/ConnectFile.class.php";
require_once "model/Product.class.php";

/**
 * Description of ProductFileDAO
 *
 * @author tarda
 */
class ProductFileDAO implements ModelInterface {

    private static $instance = NULL; // instancia de la clase
    private $connect; // conexión actual

    const FILE = "model/resource/products.txt";

    public function __construct() {
        $this->connect = new ConnectFile(self::FILE);
    }

    // singleton: patrón de diseño que crea una instancia única
    // para proporcionar un punto global de acceso y controlar
    // el acceso único a los recursos físicos
    public static function getInstance(): ProductFileDAO {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function add($product): bool {
        $result = FALSE;
        if ($this->connect->openFile("a+")) {
            fputs($this->connect->getHandle(), $product->__toString());
            $this->connect->closeFile();
            $result = TRUE;
        }
        return $result;
    }

    public function delete($id): bool {
        $result = FALSE;
        $fileData = array();

        if ($this->connect->openFile("r")) {
            while (!feof($this->connect->getHandle())) {
                $line = trim(fgets($this->connect->getHandle()));
                if ($line != "") {
                    $fields = explode(";", $line);

                    if ($id !== $fields[0]) {//guardo la line si es distinto 
                         array_push($fileData, $line . "\n");
                    } 
                }
            }
            $this->connect->closeFile();
        }
        if ($this->connect->writeFile($fileData)) {
            $result = TRUE;
        }
        return $result;
    }

    public function listAll(): array {
        $result = array();

        //abrir fichero en modo lectura
        if ($this->connect->openFile("r")) {
            while (!feof($this->connect->getHandle())) {
                $line = trim(fgets($this->connect->getHandle()));
                if ($line != "") {
                    $fields = explode(";", $line);
                    $product = new Product($fields[0], $fields[1], $fields[2], $fields[3], $fields[4]);
                    array_push($result, $product);
                }
            }
            $this->connect->closeFile();
        }
        return $result;
    }

    public function modify($product): bool {
        $result = FALSE;
        $fileData = array();

        if ($this->connect->openFile("r")) {
            while (!feof($this->connect->getHandle())) {
                $line = trim(fgets($this->connect->getHandle()));
                if ($line != "") {
                    $fields = explode(";", $line);

                    if ($product->getId() == $fields[0]) { 
                        array_push($fileData, $product->__toString());// me guarda la categoria y me lo separa 
                    } else {
                        array_push($fileData, $line . "\n");
                    }
                }
            }
            $this->connect->closeFile();
        }
        if ($this->connect->writeFile($fileData)) {
            $result = TRUE;
        }
        return $result;
    }

    public function searchById($id) {
        $product = NULL;
        // abre el fichero en modo read
        if ($this->connect->openFile("r")) {
            while (!feof($this->connect->getHandle())) {
                $line = trim(fgets($this->connect->getHandle()));
                if ($line != "") {
                    $fields = explode(";", $line);

                    if ($id == $fields[0]) {
                        $product = new Product($fields[0], $fields[1], $fields[2], $fields[3], $fields[4]);
                        break;
                    }
                }
            }
            $this->connect->closeFile();
        }

        return $product;
    }

    public function categoryInProduct($idcategory): bool {
        $result = FALSE;
        if ($this->connect->openFile("r")) {
            while (!feof($this->connect->getHandle())) {
                $line = trim(fgets($this->connect->getHandle()));
                if ($line != "") {
                    $fields = explode(";", $line);
                    if ($idcategory == $fields[4]) {
                        //print_r($fields);
                        $result = TRUE;
                        break;
                    }
                }
            }
            $this->connect->closeFile();
        }
        return $result;
    }

}
