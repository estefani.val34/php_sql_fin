<?php

require_once "model/ModelInterface.class.php";
require_once "model/persist/ConnectFile.class.php";

class EmployeeFileDAO implements ModelInterface {

    private static $instance = NULL; // instancia de la clase
    private $connect; // conexión actual

    const FILE = "model/resource/employees.txt";

    public function __construct() {
        $this->connect = new ConnectFile(self::FILE);
    }

    // singleton: patrón de diseño que crea una instancia única
    // para proporcionar un punto global de acceso y controlar
    // el acceso único a los recursos físicos
    public static function getInstance(): EmployeeFileDAO {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * select all employees from file
     * @param void
     * @return array of employee objects or array void
     */
    public function listAll(): array {
        $result = array();

        // abre el fichero en modo read
        if ($this->connect->openFile("r")) {
            while (!feof($this->connect->getHandle())) {
                $line = trim(fgets($this->connect->getHandle()));
                if ($line != "") {
                    $fields = explode(";", $line);
                    $employee = new Employee($fields[0],$fields[1],$fields[2],$fields[3]);
                 //   print_r($employee);
                    array_push($result, $employee);
                }
            }
            $this->connect->closeFile();
        }
        return $result;
    }

    /**
     * insert a employee in file
     * @param $employee employee object to insert
     * @return TRUE or FALSE
     */
    public function add($employee): bool {
        $result = FALSE;

        // abre el fichero en modo append
        if ($this->connect->openFile("a+")) {
            fputs($this->connect->getHandle(), $employee->__toString());
            $this->connect->closeFile();
            $result = TRUE;
        }

        return $result;
    }

    /**
     * select a employee by Id from file
     * @param $id string employee Id
     * @return employee object or NULL
     */
    public function searchById($id) {
        $employee = NULL;
        // abre el fichero en modo read
        if ($this->connect->openFile("r")) {
            while (!feof($this->connect->getHandle())) {
                $line = trim(fgets($this->connect->getHandle()));
                if ($line != "") {
                    $fields = explode(";", $line);

                    if ($id == $fields[0]) {
                        $employee = new Employee($fields[0], $fields[1],$fields[2], $fields[3]);
                        break;
                    }
                }
            }
            $this->connect->closeFile();
        }

        return $employee;
    }

    /**
     * update a employee in file
     * @param $employee employee object to update
     * @return TRUE or FALSE
     */
    public function modify($employee): bool {
        $result = FALSE;
        $fileData = array();

        if ($this->connect->openFile("r")) {
            while (!feof($this->connect->getHandle())) {
                $line = trim(fgets($this->connect->getHandle()));
                if ($line != "") {
                    $fields = explode(";", $line);

                    if ($employee->getId() == $fields[0]) { 
                        array_push($fileData, $employee->__toString());// me guarda la categoria y me lo separa 
                    } else {
                        array_push($fileData, $line . "\n");
                    }
                }
            }
            $this->connect->closeFile();
        }
        if ($this->connect->writeFile($fileData)) {
            $result = TRUE;
        }
        return $result;
    }

    /**
     * delete a employee in file
     * @param $id string employee Id to delete
     * @return TRUE or FALSE
     */
    public function delete($id): bool {
        $result = FALSE;
        $fileData = array();

        if ($this->connect->openFile("r")) {
            while (!feof($this->connect->getHandle())) {
                $line = trim(fgets($this->connect->getHandle()));
                if ($line != "") {
                    $fields = explode(";", $line);

                    if ($id !== $fields[0]) {//guardo la line si es distinto 
                         array_push($fileData, $line . "\n");
                    } 
                }
            }
            $this->connect->closeFile();
        }
        if ($this->connect->writeFile($fileData)) {
            $result = TRUE;
        }
        return $result;
    }

}
