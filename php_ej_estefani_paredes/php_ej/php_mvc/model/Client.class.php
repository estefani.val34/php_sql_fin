<?php
class Client {
    
    private $id;
    private $name;
    private $surname;
    private $product;
    
    #private $productList; // array of Product objects
 
    public function __construct($id=NULL, $name=NULL, $surname=NULL, $product=NULL) {
        $this->id = $id;
        $this->name = $name;
        $this->surname = $surname;
        $this->product = $product;
    }
    
    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function getSurname() {
        return $this->surname;
    }

    public function getProduct() {
        return $this->product;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setSurname($surname) {
        $this->surname = $surname;
    }

    public function setProduct($product) {
        $this->product = $product;
    }

        

    public function __toString() {
        return sprintf("%s;%s;%s;%s\n", $this->id, $this->name, $this->surname, $this->product);
    }

    
}
