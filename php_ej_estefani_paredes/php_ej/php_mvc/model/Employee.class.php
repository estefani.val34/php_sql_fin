<?php

class Employee {

    private $id;
    private $name;
    private $surname;
    private $category;

    public function __construct($id = NULL, $name = NULL, $surname = NULL,$category=NULL) {
        $this->id = $id;
        $this->name = $name;
        $this->surname = $surname;
        $this->category = $category;
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    function getSurname() {
        return $this->surname;
    }

    function setSurname($surname) {
        $this->surname = $surname;
    }
    function getCategory() {
        return $this->category;
    }

    function setCategory($category) {
        $this->category = $category;
    }

    public function __toString() {
        return sprintf("%s;%s;%s;%s\n", $this->id, $this->name, $this->surname,$this->category);
    }

}
