<?php

require_once "model/ModelInterface.class.php";
require_once "model/persist/ProductFileDAO.class.php";

/**
 * Description of ProductModel
 *
 * @author tarda
 */
class ProductModel implements ModelInterface {

    private $dataProduct;

    public function __construct() {
        $this->dataProduct = ProductFileDAO::getInstance();
    }

    public function add($product): bool {
        $result = $this->dataProduct->add($product);
        if ($result == FALSE) {
            $_SESSION['error'] = ProductMessage::ERR_DAO['insert'];
        }
        return $result;
    }

    public function delete($id): bool {
        $result = $this->dataProduct->delete($id);
        if (!$result) {
            $_SESSION['error'] = ProductMessage::ERR_DAO['delete'];
        }
        return $result;
    }

    public function listAll(): array {
        return $this->dataProduct->listAll();
    }

    public function listCategories(): array {
        $categoryModel = new CategoryModel();
        return $categoryModel->listAll();
    }

    public function modify($product): bool {
        $result = $this->dataProduct->modify($product);
        if ($result == FALSE) {//false
            $_SESSION['error'] = ProductMessage::ERR_DAO['update'];
        }
        return $result;
    }

    public function searchById($id) {
        $product = $this->dataProduct->searchById($id);
        return $product;
    }

    //le pasa una categoria; true :si hay algun producto que tenga esa categoria 
    public function categoryInProduct($idcategory): bool {
        $result = $this->dataProduct->categoryInProduct($idcategory);
        return $result;
    }

}
