<?php
require_once "model/ModelInterface.class.php";
require_once "model/persist/EmployeeFileDAO.class.php";
require_once "model/CategoryModel.class.php";

class EmployeeModel implements ModelInterface{

    private $dataEmployee;

    public function __construct() {
        $this->dataEmployee = EmployeeFileDAO::getInstance();
    }

    /**
     * select all employees
     * @param void
     * @return array of Employee objects or array void
     */
    public function listAll(): array {
        $employees = $this->dataEmployee->listAll();
        return $employees;
    }

    /**
     * insert a employee
     * @param $employee Employee object to insert
     * @return TRUE or FALSE
     */
    public function add($employee): bool {
        $result = $this->dataEmployee->add($employee);

        if ($result == FALSE) {
            $_SESSION['error'] = EmployeeMessage::ERR_DAO['insert'];
        }
        return $result;
    }

    /**
     * select a employee by Id
     * @param $id string Employee Id
     * @return Employee object or NULL
     */
    public function searchById($id) {

        $employee = $this->dataEmployee->searchById($id);
        //print_r($id);
        return $employee;
    }

    /**
     * update a employee
     * @param $employee employee object to update
     * @return TRUE or FALSE
     */
    public function modify($employee): bool {
        $result = $this->dataEmployee->modify($employee);
        if ($result == FALSE) {//false
            $_SESSION['error'] = EmployeeMessage::ERR_DAO['update'];
        }
        return $result;
    }

    /**
     * delete a employee . I can delete a employee if this have products , this is in used
     * Only can delete if employee is not in use
     * @param $id string employee Id to delete
     * @return TRUE or FALSE
     * No puedes borrar un empleado si pertenece a una categoria
     */
    public function delete($id): bool {
          $result = $this->dataEmployee->delete($id);
        if (!$result) {
            $_SESSION['error'] = EmployeeMessage::ERR_DAO['delete'];
        }
        return $result;
    }

    
    public function listCategories(): array {
        $categoryModel = new CategoryModel();
        return $categoryModel->listAll();
    }

}
