<?php

// File
//require_once "model/persist/CategoryFileDAO.class.php";
/*
 * bindParam(:id,$var,); trabaj por referencia  --> crear una variable con $var=$category->getId
 * */

// Database
require_once "model/persist/ClientDbDAO.class.php";

class ClientModel {

    private $dataClient;

    public function __construct() {
        
        $this->dataClient = ClientDbDAO::getInstance();
    }

    /**
     * insert a cleint
     * @param client client object to insert
     * @return TRUE or FALSE
     */
    public function add($client): bool {
        $result = $this->dataClient->add($client);

        if ($result == FALSE && empty($_SESSION['error'])) {
            $_SESSION['error'] = ClientMessage::ERR_DAO['insert'];
        }

        return $result;
    }

    /**
     * update a category
     * @param $category Category object to update
     * @return TRUE or FALSE
     */
    public function modify($client): bool {
        $result = $this->dataClient->modify($client);
        if ($result == FALSE) {//false
            $_SESSION['error'] = ClientMessage::ERR_DAO['update'];
        }
        return $result;
    }

    /**
     * delete a category
     * @param $id string Category Id to delete
     * @return TRUE or FALSE

      public function delete($id): bool {
      $result = $this->dataCategory->delete($id);
      if (!$result) {
      $_SESSION['error'] = ProductMessage::ERR_DAO['delete'];
      }
      return $result;
      } */
    public function delete($username): bool {
       $result = $this->dataClient->delete($username);
        if (!$result) {
            $_SESSION['error'] = ClientMessage::ERR_DAO['delete'];
        }
        return $result;
    }

    /**
     * select all clients
     * @param void
     * @return array of CLient objects or array void
     */
    public function listAll(): array {
        $clients = $this->dataClient->listAll();
  
        return $clients;
    }

    /**
     * select a client by Id
     * @param $id string client Id
     * @return client object or NULL
     */
    public function searchById($username) {
        $result = $this->dataClient->searchById($username);
        return $result;
    }
    
    public function listRoles(): array {
        $result = array(
            'Basic' => 'Basic',
            'Advanced' => 'Advanced',
            );
        return $result;
    }

}
