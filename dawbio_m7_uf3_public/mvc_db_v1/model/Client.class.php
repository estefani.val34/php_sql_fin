<?php

class Client {

    private $username;
    private $age;
    private $role;
    private $active;
    private $password;

    public function __construct($username=NULL, $password=NULL, $age=NULL, $role=NULL, $active=NULL) {
        $this->username = $username;
        $this->age = $age;
        $this->role = $role;
        $this->active = $active;
        $this->password = $password;
    }

    public function getUsername() {
        return $this->username;
    }

    public function getAge() {
        return $this->age;
    }

    public function getRole() {
        return $this->role;
    }

    public function getActive() {
        return $this->active;
    }

    public function getPassword() {
        return $this->password;
    }

    public function setUsername($username) {
        $this->username = $username;
    }

    public function setAge($age) {
        $this->age = $age;
    }

    public function setRole($role) {
        $this->role = $role;
    }

    public function setActive($active) {
        $this->active = $active;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    public function __toString() {
        return sprintf("%s;%s;%s;%s;%s\n", $this->username, $this->age,
                        $this->role, $this->active, $this->password);
    }

}
