<?php

require_once "controller/ControllerInterface.php";
require_once "view/ClientView.class.php";
require_once "model/ClientModel.class.php";
require_once "model/Client.class.php";
require_once "util/ClientMessage.class.php";
require_once "util/ClientFormValidation.class.php";

class ClientController implements ControllerInterface {

    private $view;
    private $model;

    public function __construct() {
        // carga la vista
        $this->view = new ClientView();

        // carga el modelo de datos
        $this->model = new ClientModel();
    }

    // carga la vista según la opción o ejecuta una acción específica
    public function processRequest() {

        $request = NULL;
        $_SESSION['info'] = array();
        $_SESSION['error'] = array();

        // recupera la acción de un formulario
        if (filter_has_var(INPUT_POST, 'action')) {
            $request = filter_has_var(INPUT_POST, 'action') ? filter_input(INPUT_POST, 'action') : NULL;
        }
        // recupera la opción de un menú
        else {
            $request = filter_has_var(INPUT_GET, 'option') ? filter_input(INPUT_GET, 'option') : NULL;
        }

        switch ($request) {
            case "form_add": // el botón de reset en el formulario de insertar categoría entra en este caso
                $this->formAdd();
                break;
            case "add":
                $this->add();
                break;
            case "list_all":
                $this->listAll();
                break;
            case "form_search":// serach modify i dellete 
                $this->formSearch();
                break;
            case "search":// serach modify i dellete 
                $this->searchById();
                break;
            case "form_modify":// serach modify i dellete 
                $this->formModify();
                break;
            case "modify":// serach modify i dellete 
                $this->modify();
                break;
            case "delete":
                $this->delete();
                break;
            default:
                $this->view->display();
        }
    }

    // carga el formulario de insertar client
    public function formAdd() {
        $roles = $this->model->listRoles();
        $this->view->display("view/form/ClientFormAdd.php", null, $roles);
    }

    // ejecuta la acción de insertar client
    public function add() {
        $clientValid = ClientFormValidation::checkData(ClientFormValidation::ADD_FIELDS);

        if (empty($_SESSION['error'])) {
            $client = $this->model->searchById($clientValid->getUsername());

            if (is_null($client)) {
                $result = $this->model->add($clientValid);

                if ($result == TRUE) {
                    $_SESSION['info'] = ClientMessage::INF_FORM['insert'];
                    $clientValid = NULL;
                }
            } else {
                $_SESSION['error'] = ClientMessage::ERR_FORM['exists_username'];
            }
        }
        $roles = $this->model->listRoles();
        $this->view->display("view/form/ClientFormAdd.php", $clientValid, $roles);
    }

    // ejecuta la acción de mostrar todas las client
    public function listAll() {
        $clients = $this->model->listAll();
        print_r($clients);
        if (empty($_SESSION['error'])) {
            if (!empty($clients)) {
                $_SESSION['info'] = ClientMessage::INF_FORM['found'];
            } else {
                $_SESSION['error'] = ClientMessage::ERR_FORM['not_found'];
            }
        }

        $this->view->display("view/form/ClientList.php", $clients);
    }

    // ejecuta la acción de eliminar categoría 
    public function delete() {
        $clientValid = ClientFormValidation::checkData(ClientFormValidation::DELETE_FIELDS);

        if (empty($_SESSION['error'])) {
            $client = $this->model->searchById($clientValid->getUsername());

            if (!is_null($client)) {


                $result = $this->model->delete($clientValid->getUsername());

                if ($result == TRUE) {
                    $_SESSION['info'] = ClientMessage::INF_FORM['delete'];
                    $clientValid = NULL; //para que me muestre vacia la lista
                }
            } else {
                $_SESSION['error'] = ClientMessage::ERR_FORM['not_exists_username'];
            }
        }
        $roles = $this->model->listRoles();
        $this->view->display("view/form/ClientFormModify.php", $clientValid, $roles);
    }
//formulari per modificar
    public function formModify($clientValid) {
        $roles = $this->model->listRoles();
        $this->view->display("view/form/ClientFormModify.php", $clientValid, $roles);
    }
// el metode per modificar
    
    public function modify() {
        $clientValid = ClientFormValidation::checkData(ClientFormValidation::MODIFY_FIELDS);

        if (empty($_SESSION['error'])) {
            $client = $this->model->searchById($clientValid->getUsername());

            if (!is_null($client)) {
                $result = $this->model->modify($clientValid);

                if ($result == TRUE) {
                    $_SESSION['info'] = ClientMessage::INF_FORM['update'];
                    $clientValid = NULL;
                }
            } else {
                $_SESSION['error'] = ClientMessage::ERR_FORM['not_exists_username'];
            }
        }

        $roles = $this->model->listRoles();
        $this->view->display("view/form/ClientFormModify.php", $clientValid, $roles);
    }

    //form de search
    public function formSearch() {
        $roles = $this->model->listRoles();
        $this->view->display("view/form/ClientFormSearch.php", null, $roles);
    }

 // busca el username en la tabla 
    public function searchById() {
        $clientValid = ClientFormValidation::checkData(ClientFormValidation::SEARCH_FIELDS);
        //print_r($userValid);
        if (empty($_SESSION['error'])) {
            $client = $this->model->searchById($clientValid->getUsername());
            //   print_r($client->getActive());
            if (!is_null($client)) {
                $_SESSION['info'] = ClientMessage::INF_FORM['found'];
                $clientValid = $client;
            } else {
                $_SESSION['error'] = ClientMessage::ERR_FORM['not_found'];
            }
        }

        if (!empty($_SESSION['error'])) {
            $roles = $this->model->listRoles();
            $this->view->display("view/form/ClientFormSearch.php", $clientValid, $roles);
        } else {
            $this->formModify($clientValid);
        }
    }

}
